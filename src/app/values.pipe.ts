import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'values'
})
export class ValuesPipe implements PipeTransform {

  strings = {
    amount: 'Holding',
    buyat: 'Buy at',
    currentbuy: 'Buy price',
    currentsell: 'Sell price',
    highbb: 'BB up limit',
    longema: 'Long EMA',
    lowbb: 'BB low limit',
    price: 'Current price',
    price_ask: 'Min ask',
    price_bid: 'Min bid',
    sellat: 'Sell at',
    shortema: 'Short EMA',
    sma: 'SMA'
  };
  transform(value: any, args?: any): any {
    const keys: Array<any> = Object.keys(value),
      data: Array<any> = [];

    keys.forEach((key: string) => {
      if (key !== 'pair') {
        data.push({
          display: this.strings[key] || key,
          key: this.strings[key] || key,
          value: value[key]
        });
      }
    });
    return data.sort((a, b) => {
      if (a.value > b.value) {
        return -1;
      }
      return 1;
    });
  }

}

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { SocketService } from './socket.service';
import 'rxjs/Rx';

const CHAT_URL = 'ws://173.249.3.27:5001/';

export interface Message {
  pair: string;
  message: string;
}

@Injectable()
export class ChatService {
  public messages: Subject<any>;

  constructor(wsService: SocketService) {
    this.messages = <Subject<Message>>wsService
      .connect(CHAT_URL)
      .map((response: MessageEvent): any => JSON.parse(response.data));
  }
}

import { Component } from '@angular/core';
import { SocketService } from './socket.service';
import { ChatService } from './chat.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ SocketService, ChatService ]
})
export class AppComponent {
  title = 'Gunbot pairs';
  dec = 10;
  pairs: Array<any> = [];
  toSort = true;

  constructor(private chatService: ChatService) {
    chatService.messages
      .subscribe(msg => {
        if (msg.pair) {
          this.toSort = false;
          if (msg.event !== 'getting_balances') {
            if (!this.hasPair(msg.pair)) {
              this.pairs.push({
                pair: msg.pair
              });
              this.toSort = true;
            }
            if (msg.event === 'getting_ema_1') {
              this.pairs[this.findPair(msg.pair)].longema = msg.data.ema.toFixed(this.dec);
            } else if (msg.event === 'getting_ema_2') {
              this.pairs[this.findPair(msg.pair)].shortema = msg.data.ema.toFixed(this.dec);
            } else if (msg.event === 'getting_ta') {
              this.pairs[this.findPair(msg.pair)].highbb = msg.data.price_high_bb.toFixed(this.dec);
              this.pairs[this.findPair(msg.pair)].lowbb = msg.data.price_low_bb.toFixed(this.dec);
              this.pairs[this.findPair(msg.pair)].sma = msg.data.price_sma.toFixed(this.dec);
            } else if (msg.event === 'getting_public_trade_history') {
              this.pairs[this.findPair(msg.pair)].buyat = msg.data.price_buy.toFixed(this.dec);
              this.pairs[this.findPair(msg.pair)].sellat = msg.data.price_sell.toFixed(this.dec);
            } else if (msg.event === 'getting_order_book') {
              this.pairs[this.findPair(msg.pair)].currentbuy = msg.data.price_ask.toFixed(this.dec);
              this.pairs[this.findPair(msg.pair)].currentsell = msg.data.price_bid.toFixed(this.dec);
            } else {
              console.log('not yet dealt with ', msg.event, msg);
              this.pairs[this.findPair(msg.pair)] = { ...this.pairs[this.findPair(msg.pair)], ...msg.data };
            }
            if (this.toSort) {
              this.pairs.sort((a, b) => {
                if (a.pair > b.pair) {
                  return 1;
                }
                return -1;
              });
            } else {
              this.pairs[this.findPair(msg.pair)] = {
                ...this.pairs[this.findPair(msg.pair)]
              };
            }
          }
        }
      });
  }

  findPair(pair: string) {
    return this.pairs.findIndex(el => el.pair === pair);
  }

  hasPair(pair: string) {
    return this.findPair(pair) !== -1;
  }

}
